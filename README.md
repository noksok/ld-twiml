### Repository details

Module that adds support for LeadML (=implementation of Twilio's TwiML) instructions to an inbound switch

### Required versions

- Asterisk 11
- FreePBX 2.11
- PHP 5.3

### Creating a package

- Do the code changes
- Update module.xml file by changing the value inside version tags and add a new line to changelog
- Create a new package with `tar -zcvf ld_twiml-x.x.x.tar.gz ld_twiml`. Note that you need to change x.x.x to the correct version number
- Upload the package to bitbucket so that it is available on the download page

### Deployment

See the confluence page for help https://confluence.leaddesk.ch/display/TEL/Freepbx+modules


### Tests

Install dependencies with composer and then run the test suite

- `composer install`
- `./vendor/bin/phpunit tests`

Bitbucket pipelines runs unit tests and linting for php 5.3, 7.4 and 8.0 automatically
