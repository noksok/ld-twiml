<?php
// LD_TWIML 1.0
if (!defined('FREEPBX_IS_AUTH')) {
    die('No direct script access allowed');
}
//This file is part of LeadDesk

global $db;
global $amp_conf;

$base_source = dirname(__FILE__);
$agibin_dest = isset($amp_conf['ASTAGIDIR']) ? $amp_conf['ASTAGIDIR']:'/var/lib/asterisk/agi-bin';

exec("cp -rf " . $base_source . "/agi-bin/agi_twiml.php " . $agibin_dest . " 2>&1");

if (substr($agibin_dest, -1) !== "/") {
    $agibin_dest .= "/";
}
// Remove /var/lib/asterisk/agi-bin/ld_twiml/ folder and copy new content in its place
exec("rm -rf " . $agibin_dest . "ld_twiml/ 2>&1");
exec("cp -rf " . $base_source . "/agi-bin/ld_twiml/ " . $agibin_dest . "ld_twiml 2>&1");

$autoincrement = (($amp_conf["AMPDBENGINE"] == "sqlite") || ($amp_conf["AMPDBENGINE"] == "sqlite3")) ? "AUTOINCREMENT" : "AUTO_INCREMENT";
$sql = "CREATE TABLE IF NOT EXISTS ld_twiml (
    ld_twiml_id INTEGER NOT NULL PRIMARY KEY $autoincrement,
    description VARCHAR( 50 ),
    client_id INT,
    protocol VARCHAR( 10 ),
    url VARCHAR( 255 ),
    auth VARCHAR( 255 ),
    post_dest VARCHAR( 255 )
)";
$check = $db->query($sql);
if (DB::IsError($check)) {
    die_freepbx("Can not create ld_twiml table");
}
/*
// Update from version 1.0.0
$db->query("ALTER TABLE ld_twiml ADD recording_id INT");

// Update from version 1.0.5
$db->query("ALTER TABLE ld_twiml ADD comment VARCHAR( 255 )");

// Update from version 1.0.5
$db->query("ALTER TABLE ld_twiml ADD askdigits BOOL");

// Update from version 1.0.5
$db->query("ALTER TABLE ld_twiml ADD queue_ext VARCHAR( 40 )");
*/
$results = array();
$sql = "SELECT ld_twiml_id, post_dest FROM ld_twiml";
$results = $db->getAll($sql, DB_FETCHMODE_ASSOC);
if (!DB::IsError($results)) { // error - table must not be there
    foreach ($results as $result) {
        $old_dest = $result['post_dest'];
        $ld_twiml_id = $result['ld_twiml_id'];

        $new_dest = merge_ext_followme(trim($old_dest));
        if ($new_dest != $old_dest) {
            $sql = "UPDATE ld_twiml SET post_dest = '$new_dest' WHERE ld_twiml_id = $ld_twiml_id  AND post_dest = '$old_dest'";
            $results = $db->query($sql);
            if (DB::IsError($results)) {
                die_freepbx($results->getMessage());
            }
        }
    }
}
?>
