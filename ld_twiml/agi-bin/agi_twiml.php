#!/usr/bin/php -q
<?php
require_once(__DIR__ . "/ld_twiml/verbs/Dial.php");
require_once(__DIR__ . "/ld_twiml/verbs/Say.php");
require_once(__DIR__ . "/ld_twiml/verbs/Pause.php");
require_once(__DIR__ . "/ld_twiml/verbs/Play.php");
require_once(__DIR__ . "/ld_twiml/verbs/Language.php");
require_once(__DIR__ . "/ld_twiml/utils/AgiCommand.php");
require_once(__DIR__ . "/ld_twiml/utils/XmlLogger.php");
require_once(__DIR__ . "/ld_twiml/utils/SoundFiles.php");
require_once(__DIR__ . "/ld_twiml/utils/Utils.php");


class LeadmlProcessor
{
    private static $FILE_STORAGE_FOLDER = "/tmp/";

    public function __construct($agi, $calledFrom, $calledTo, $uniqueId, $clientId, $clientAuth, $callId)
    {
        $this->agi = $agi;
        $this->calledFrom = $calledFrom;
        $this->calledTo = $calledTo;
        $this->uniqueId = $uniqueId;
        $this->clientId = $clientId;
        $this->clientAuth = $clientAuth;
        $this->callId = $callId;

        $this->callerIdChanged = false;

        $this->xmlLogger = new \Utils\XmlLogger($this->callId, $this->uniqueId, $this->clientId, $this->calledFrom, $this->calledTo);
        $this->agiCommand = new \Utils\AgiCommand($this->agi);
    }

    public function process($url, $postVars=null)
    {
        if ($postVars === null) {
            $postVars = $this->getDefaultPostVars();
        }

        $document = $this->fetchXmlDocument($url, $postVars);
        $this->processNode($document, $url);
    }

    private function fetchXmlDocument($url, $postVars)
    {
        $document = new DOMDocument();
        $document->preserveWhiteSpace = FALSE;

        $fileContent = $this->sendRequest($url, $postVars);

        $fileContent = strtolower($fileContent);
        $fileContent = str_replace("\n" , "", $fileContent);
        $fileContent = str_replace("\r" , "", $fileContent);

        $document->loadXML($fileContent);

        return $document;
    }

    private function sendRequest($url, $postVars = "") {
        $auth = "client_id=" . $this->clientId . "&ld_client_auth=" . $this->clientAuth;

        if (strpos($url, "?") !== false) {
            $url .= "&" . $auth;
        } else {
            $url .= "?" . $auth;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        if ($postVars != "") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postVars);
            $this->agiCommand->log("CURL " . $url . " vars: " . $postVars);

        }
        $return = curl_exec($ch);

        return $return;
    }

    private function getDefaultPostVars()
    {
        return "From=" . urlencode($this->calledFrom) . "&To=" . urlencode($this->calledTo) . "&CallSid=" . md5($this->uniqueId) . "&AccountSid=" . $this->clientId;
    }

    private function createPostVars($extras)
    {
        $postVars = $this->getDefaultPostVars();

        foreach ($extras as $name => $value) {
            $postVars .= "&{$name}={$value}";
        }

        return $postVars;
    }

    private function processNode(DOMNode $domNode, $parentUrl) {

        foreach ($domNode->childNodes as $node)
        {
            $nodeName = strtolower($node->nodeName);
            $nodeValue = str_replace("\n", " ", $node->nodeValue);
            switch ($nodeName) {
                case "#text":
                    continue 2;

                case "say":
                    $this->agiCommand->answerCall();

                    $say = new \Verbs\Say($node);
                    $this->xmlLogger->write($say->getNodeData());

                    $fileBaseNameWithPath = self::$FILE_STORAGE_FOLDER . $say->getFileBaseName();
                    $downloadUrl = $say->getDownloadUrl();

                    if (\Utils\SoundFiles::filesExist($fileBaseNameWithPath) === false) {
                        $this->agiCommand->log("Downloading {$fileBaseNameWithPath} from {$downloadUrl}");
                        \Utils\SoundFiles::getAndConvert($fileBaseNameWithPath, $downloadUrl);
                    }

                    $this->agiCommand->playFile($fileBaseNameWithPath);

                    break;

                case "pause":
                    $this->agiCommand->answerCall();

                    $pause = new \Verbs\Pause($node);
                    $this->xmlLogger->write($pause->getNodeData());
                    $this->agiCommand->wait($pause->getTimeout());

                    break;

                case "dial":
                    $this->agiCommand->answerCall();

                    $dial = new \Verbs\Dial($node);
                    $command = $dial->getAsteriskCommand();

                    if ($command !== "") {
                        // Caller id manipulation is done before any other action is taken
                        $this->agiCommand->addPostfix($this->calledFrom, $dial->getCallerIdPostfix());

                        $this->xmlLogger->write($dial->getNodeData());
                        $resp = $this->agiCommand->send($command);
                        $this->agiCommand->log("Status after call transfer: " . json_encode($resp));

                        // Asterisk response 0 means no answer/timeout. Execution can continue if we get more XML
                        // from the url set in Dial verb's action attribute
                        $dialActionUrl = $dial->getAttribute("action");
                        if (is_numeric($resp["result"]) && intval($resp["result"]) === 0 && !is_null($dialActionUrl)) {

                            $dialCallStatus = $dial->getDialCallStatusForAsteriskDialResult(intval($resp["result"]));

                            if ($dialCallStatus !== null) {
                                $this->agiCommand->log("Continuing after call transfer with status: {$dialCallStatus}");
                                $extraPostVars = array("DialCallStatus" => urlencode($dialCallStatus));
                                $this->process($dialActionUrl, $this->createPostVars($extraPostVars));
                            }
                        }
                    }
                    exit;
                    break;

                // TODO: Fix this idiotic thing
                case "gather":
                    $this->agiCommand->answerCall();
                    $this->agiCommand->log("Starting to gather");

                    if ($node->hasAttribute("numdigits")) {
                        $digitCount = $node->getAttribute("numdigits");

                    } else {
                        $digitCount = 1;
                        
                    }

                    if ($node->hasAttribute("action")) {
                        $gatherURL = \Utils\getAbsoluteUrl($node->getAttribute("action"), $parentUrl);

                    } else {
                        $gatherURL = $parentUrl;
                        
                    }

                    if ($node->hasAttribute("finishonkey")) {
                        $finishKey = $node->getAttribute("finishonkey");

                    } else {
                        $finishKey = "#";
                        
                    }

                    if ($node->hasAttribute("timeout")) {
                        $timeoutValue = $node->getAttribute("timeout");

                    } else {
                        $timeoutValue = 10;
                        
                    }

                    $this->xmlLogger->writeFromXml($node);

                    if($node->hasChildNodes()) {
                        if ($this->processGatherNode($node, $parentUrl, $gatherURL, $digitCount, $timeoutValue, $finishKey)) {
                            exit;

                        }

                    }

                    break;

                case "redirect":
                    $this->agiCommand->answerCall();
                    $this->xmlLogger->writeFromXml($node);
                    $this->process($nodeValue, $this->getDefaultPostVars());

                    break;

                case "hangup":
                    $this->agiCommand->answerCall();
                    $this->xmlLogger->writeFromXml($node);
                    $this->agiCommand->hangupCall();

                    break;

                case "reject":
                    $this->xmlLogger->writeFromXml($node);
                    $this->agiCommand->hangupCall();

                    break;

                case "play":
                    $this->agiCommand->answerCall();
                    $play = new \Verbs\Play($node, $this->clientId);
                    $this->xmlLogger->write($play->getNodeData());

                    $fileBaseNameWithPath = self::$FILE_STORAGE_FOLDER . $play->getFileBaseName();
                    $downloadUrl = $play->getDownloadUrl();

                    if (\Utils\SoundFiles::filesExist($fileBaseNameWithPath) === false) {
                        $this->agiCommand->log("Downloading {$fileBaseNameWithPath} from {$downloadUrl}");
                        \Utils\SoundFiles::getAndConvert($fileBaseNameWithPath, $downloadUrl);
                    }

                    $this->agiCommand->playFile($fileBaseNameWithPath);

                    break;

                case "language":
                    $language = new \Verbs\Language($node);
                    $this->xmlLogger->write($language->getNodeData());

                    $this->agiCommand->setLanguage($language->getLanguage());

                    break;

                default:
                    // Response is supposed to be skipped so here we ignore it so that we get less spam in the logs
                    if ($nodeName !== "response") {
                        $this->agiCommand->log("Node not recognised: {$nodeName}");
                    }
            }


            if ($node->hasChildNodes() && (strtolower($nodeName) != "gather") && (strtolower($nodeName) != "dial")) {
                $this->processNode($node, $parentUrl);
            }

        }
    }

    // TODO: Bake this into the normal XML processing loop above
    public function processGatherNode(DOMNode $domNode, $parentURL, $gather = "", $digitCount = 1, $timeoutValue = 10, $finishKey = "#") {
        $this->agiCommand->log("Processing gather node");

        $result = array('code'=> -1, 'result'=> -1, 'timeout'=> false, 'data'=> '');
        $gatherDigitCurrent = "";
     
        foreach ($domNode->childNodes as $node)
        {
            $nodeName = $node->nodeName;
            $nodeValue = str_replace("\n", " ", $node->nodeValue);
            switch ($nodeName) {
                case "#text":
                    break;

                case "say":
                    $this->agiCommand->answerCall();

                    $say = new \Verbs\Say($node);
                    $this->xmlLogger->write($say->getNodeData());

                    $fileBaseNameWithPath = self::$FILE_STORAGE_FOLDER . $say->getFileBaseName();
                    $downloadUrl = $say->getDownloadUrl();

                    if (\Utils\SoundFiles::filesExist($fileBaseNameWithPath) === false) {
                        $this->agiCommand->log("Downloading {$fileBaseNameWithPath} from {$downloadUrl}");
                        \Utils\SoundFiles::getAndConvert($fileBaseNameWithPath, $downloadUrl);
                    }

                    $result = $this->agiCommand->playFile($fileBaseNameWithPath);

                    break;

                case "pause":
                    $this->agiCommand->answerCall();

                    $pause = new \Verbs\Pause($node);
                    $this->xmlLogger->write($pause->getNodeData());
                    $result = $this->agiCommand->wait($pause->getTimeout());

                    break;

                case "play":
                    $this->agiCommand->answerCall();
                    $play = new \Verbs\Play($node, $this->clientId);
                    $this->xmlLogger->write($play->getNodeData());

                    $fileBaseNameWithPath = self::$FILE_STORAGE_FOLDER . $play->getFileBaseName();
                    $downloadUrl = $play->getDownloadUrl();

                    if (\Utils\SoundFiles::filesExist($fileBaseNameWithPath) === false) {
                        $this->agiCommand->log("Downloading {$fileBaseNameWithPath} from {$downloadUrl}");
                        \Utils\SoundFiles::getAndConvert($fileBaseNameWithPath, $downloadUrl);
                    }

                    $result = $this->agiCommand->playFile($fileBaseNameWithPath);

                    break;

                default:
                    $this->agiCommand->log("Node not recognised: {$nodeName}");

            }

            if (($result["code"] == 200) && ($result["result"] > 1)) {
                if (chr($result["result"]) == $finishKey) {
                    $gatherDigitCurrent = "";
                    $digitCount = 0;

                } else {
                    $gatherDigitCurrent .= chr($result["result"]);

                }
                
            }

            if (strlen($gatherDigitCurrent) >= $digitCount) {
                $this->agiCommand->log("Child node processing ending, got gather digits already.");
                break;

            }

        }

        $timeoutCounter = time();

        while (strlen($gatherDigitCurrent) < $digitCount) {
            $result = $this->agiCommand->wait(1000);
            
            if (($result["code"] == 200) && ($result["result"] > 1)) {

                $timeoutCounter = time();

                if (chr($result["result"]) == $finishKey) {
                    $digitCount = strlen($gatherDigitCurrent);

                } else {
                    $gatherDigitCurrent = $gatherDigitCurrent . chr($result["result"]);

                }
                
            }

            if (time() - $timeoutCounter > $timeoutValue) {
                break;
            }
            
        }

        if (strlen($gatherDigitCurrent) >= $digitCount) {
            $this->agiCommand->log("Gather succesfully ended. Got digits: {$gatherDigitCurrent}");
        } else {
            $this->agiCommand->log("Gather failed. Got " . strlen($gatherDigitCurrent) . "/" . $digitCount . " digits: " . $gatherDigitCurrent);
        }

        $extraPostVars = array("Digits" => urlencode($gatherDigitCurrent));
        $this->process($gather, $this->createPostVars($extraPostVars));

        // Is this really needed
        return (strlen($gatherDigitCurrent) >= $digitCount);
    }
}

/**
 * Main entry point into the agi process
 *
 * @param agispeedy_agi $agi instance of the agispeedy's agispeedy_agi class
 * @return null
 */
function agi_main(&$agi)
{
    ini_set("log_errors", 1);
    ini_set("error_log", "/tmp/php-error.log");

    // $agi->input holds all the default parameters which Asterisk sends to every agi script
    $calledFrom = $agi->input["agi_callerid"];
    $calledTo = $agi->input["agi_dnid"];
    $uniqueId = $agi->input["agi_uniqueid"];

    // $agi->param holds the parameters that are defined in Aasterisk dialplan where the agi function is called
    $urlProtocol = $agi->param["protocol"];
    $urlPath = $agi->param["url"];
    $clientId = $agi->param["client_id"];
    $clientAuth = $agi->param["auth"];
    $callId = $agi->param["call_id"];

    // Construct the initial URL
    $fullUrl = $urlProtocol . "://" . $urlPath;

    $processor = new LeadmlProcessor($agi, $calledFrom, $calledTo, $uniqueId, $clientId, $clientAuth, $callId);
    $processor->process($fullUrl);
}
