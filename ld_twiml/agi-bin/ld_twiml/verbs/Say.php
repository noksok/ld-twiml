<?php
namespace Verbs;

require_once(__DIR__ . "/BaseVerb.php");

class Say extends BaseVerb
{
    private static $TTS_API_URL = "http://api.voicerss.org/?key=73321129a8b34b239eadad092cc52a68&c=WAV&f=8khz_16bit_mono";
    private static $DEFAULT_LANGUAGE = "en-us";

    private static $SHORT_LANGUAGE_CODES = array(
        'ca' => 'ca-es',
        'da' => 'da-dk',
        'de' => 'de-de',
        'en' => 'en-us',
        'es' => 'es-es',
        'fi' => 'fi-fi',
        'fr' => 'fr-fr',
        'it' => 'it-it',
        'ja' => 'ja-jp',
        'ko' => 'ko-kr',
        'nb' => 'nb-no',
        'nl' => 'nl-nl',
        'pl' => 'pl-pl',
        'pt' => 'pt-pt',
        'ru' => 'ru-ru',
        'sv' => 'sv-se',
        'zh' => 'zh-cn',
    );

    private static $SUPPORTED_LANGUAGE_CODES = array(
        'ca-es',
        'da-dk',
        'de-de',
        'en-au',
        'en-ca',
        'en-gb',
        'en-in',
        'en-us',
        'es-es',
        'es-mx',
        'fi-fi',
        'fr-ca',
        'fr-fr',
        'it-it',
        'ja-jp',
        'ko-kr',
        'nb-no',
        'nl-nl',
        'pl-pl',
        'pt-br',
        'pt-pt',
        'ru-ru',
        'sv-se',
        'zh-cn',
        'zh-hk',
        'zh-tw',
    );

    public function getText()
    {
        return $this->getValue();
    }

    public function getLanguage()
    {
        $language = self::$DEFAULT_LANGUAGE;
        $attributeValue = $this->getAttribute("language");

        if (!is_null($attributeValue)) {
            if (in_array($attributeValue, self::$SUPPORTED_LANGUAGE_CODES)) {
                $language = $attributeValue;
            } else if (array_key_exists($attributeValue, self::$SHORT_LANGUAGE_CODES)) {
                $language = self::$SHORT_LANGUAGE_CODES[$attributeValue];
            }
        }

        return $language;
    }

    public function getFileBaseName()
    {
        return md5($this->getText() . $this->getLanguage()) . "_tts";
    }

    public function getDownloadUrl()
    {
        return self::$TTS_API_URL . "&hl=" . urlencode($this->getLanguage()) . "&src=" . urlencode($this->getText());
    }
}
