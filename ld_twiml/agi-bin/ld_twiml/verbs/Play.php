<?php
namespace Verbs;

require_once(__DIR__ . "/BaseVerb.php");

class Play extends BaseVerb
{

    public function __construct($node, $clientId)
    {
        $this->clientId = $clientId;
        parent::__construct($node);
    }

    public function getDownloadUrl()
    {
        return $this->getValue();
    }

    public function getFileBaseName()
    {
        $url_parts = explode("/", $this->getValue());
        $filename = null;

        // Try to get the last part of the url. For flow it should be xxxxxxxx.mp3
        // Use client id and the name in the url for creating a new filename
        // It can then be used for checking if we already have the file on disk
        if (count($url_parts) > 0) {
            $last_part = $url_parts[count($url_parts) - 1];

            if (substr($last_part, -4) === ".mp3") {
                $filename = md5($this->clientId . "_" . str_replace(".mp3", "", $last_part)) . "_download";
            }
        }

        if ($filename === null) {
            $filename = md5(rand(1 , 10000000)) . "_download";
        }

        return $filename;
    }
}
