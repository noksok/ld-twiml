<?php
namespace Verbs;

require_once(__DIR__ . "/BaseVerb.php");

class Language extends BaseVerb
{
    private static $DEFAULT_LANGUAGE = "en";

    // Mapping between the values supported in the XML and the language values that asterisk understands
    private static $SUPPORTED_LANGUAGE_CODES = array(
        'en' => 'en',
        'en-us' => 'en',
        'da-dk' => 'da-dk',
        'de-de' => 'de-de',
        'fi-fi' => 'fi-fi',
        'nl-nl' => 'nl-nl',
        'no-no' => 'no-no',
        'sv-se' => 'sv-se',
        'es-es' => 'es-es',
        'it-it' => 'it-it',
        'fr-fr' => 'fr-fr'
    );

    public function getLanguage()
    {
        $language = self::$DEFAULT_LANGUAGE;
        $userLanguage = $this->getValue();

        if ($userLanguage && array_key_exists($userLanguage, self::$SUPPORTED_LANGUAGE_CODES)) {
            $language = self::$SUPPORTED_LANGUAGE_CODES[$userLanguage];
        }

        return $language;
    }
}
