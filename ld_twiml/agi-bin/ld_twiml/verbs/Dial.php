<?php
namespace Verbs;

require_once(__DIR__ . "/BaseVerb.php");

class Dial extends BaseVerb
{

    // List of supported child nodes and how many of them we support
    public static $SUPPORTED_CHILDREN = array(
        "number" => array("max_consecutive" => 10),
        "queue" => array("max_consecutive" => 1)
    );

    private static $DIAL_CALL_STATUS_MAPPING = array(
        0 => "no-answer"
    );

    // What kind of timeout values we support when transferring the call
    private static $DEFAULT_TIMEOUT = 30;
    private static $MINIMUM_TIMEOUT = 5;
    private static $MAXIMUM_TIMEOUT = 600;


    /**
     * Get Asterisk command for transferring the call to a number or multiple numbers
     *
     * @param array $numbers phone numbers where the call will be transferred to
     * @param int $timeout timeout for the transferred call
     * @return string Asterisk command for executing the transfer
     */
    private static function getTransferToNumbersCommand($numbers, $timeout)
    {
        $destination_string = "";
        foreach ($numbers as $number) {
            if ($destination_string !== "") {
                $destination_string .= "&";
            }

            $destination_string .= "local/" . self::fixPhoneNumber($number) . "@from-internal";
        }

        return "EXEC DIAL " . $destination_string . "," . $timeout;
    }

    /**
     * Get Asterisk command for transferring the call to a queue
     *
     * @param string $extension queue extension where the call should be transferred to
     * @return string Asterisk command for executing the transfer
     */
    private static function getTransferToQueueCommand($extension)
    {
        return "EXEC Goto ext-queues," . $extension . ",1";
    }

    /**
     * Fix numbers starting with + to start with 00
     *
     * @param string $number phone number to be fixed
     * @return string fixed number
     */
    private static function fixPhoneNumber($number)
    {
        if (substr($number, 0, 1) === "+") {
            return "00" . substr($number, 1);
        } else {
            return $number;
        }
    }

    /**
     * Get timeout attribute's value and if needed fix it to be within acceptable bounds
     *
     * @return int value for timeout
     */
    private function getTimeout()
    {
        $timeout = self::$DEFAULT_TIMEOUT;

        $attribute_value = $this->getAttribute("timeout");

        if (is_numeric($attribute_value)) {
            $timeout = intval($attribute_value);

            // Enforce minimum and maximum timeout values
            if ($timeout < self::$MINIMUM_TIMEOUT) {
                $timeout = self::$MINIMUM_TIMEOUT;
            } elseif ($timeout > self::$MAXIMUM_TIMEOUT) {
                $timeout = self::$MAXIMUM_TIMEOUT;
            }
        }

        return $timeout;

    }

    /**
     * Create an Asterisk command based on the XML node's data
     *
     * @return string Asterisk command to be ran
     */
    public function getAsteriskCommand()
    {
        $command = "";

        // When using Dial directly without child nodes then the value is handled
        // the same way as if Dial had one Number node as a child so here we default that case to Number type
        $type = $this->hasChildren() ? $this->getChildrenType() : "number";
        $values = $this->getAllValues();

        switch ($type) {
            case "number":
                // Get transfer command if we have 1 or more numbers
                if (count($values) >= 1) {
                    $command = self::getTransferToNumbersCommand($values, $this->getTimeout());
                }

                break;

            case "queue":
                $command = self::getTransferToQueueCommand($values[0]);

                break;

            default:
                break;
        }

        return $command;

    }

    /**
     * Transform Asterisk Dial command return code to TwiML DialCallStatus
     *
     * @param int $asterisk_code Asterisk return code for DIAL command
     * @return string|null value matching $asterisk_code in $DIAL_CALL_STATUS_MAPPING or null if no match is found
     */
    public function getDialCallStatusForAsteriskDialResult($asterisk_code)
    {
        if (array_key_exists($asterisk_code, self::$DIAL_CALL_STATUS_MAPPING)) {
            return self::$DIAL_CALL_STATUS_MAPPING[$asterisk_code];
        } else {
            return null;
        }
    }

    /**
     * Return attribute callerIdPostfix if it's present and valid
     *
     * @return string|null return attribute value if it was valid or null if it's empty or invalid
     */
    public function getCallerIdPostfix()
    {
        $value = $this->getAttribute("calleridpostfix");

        if (!is_null($value) && is_numeric($value) && strlen($value) === 2 && intval($value) >= 0) {
            return $value;
        } else {
            return null;
        }
    }
}
