<?php
namespace Verbs;

require_once(__DIR__ . "/BaseVerb.php");

class Pause extends BaseVerb
{
    private static $DEFAULT_TIMEOUT = 1;
    private static $MINIMUM_TIMEOUT = 0;

    public function getTimeout()
    {
        $timeout = self::$DEFAULT_TIMEOUT;

        $attribute_value = $this->getAttribute("length");

        if (is_numeric($attribute_value)) {
            $timeout = intval($attribute_value);

            // Enforce minimum timeout
            if ($timeout < self::$MINIMUM_TIMEOUT) {
                $timeout = self::$MINIMUM_TIMEOUT;
            }
        }

        return $timeout;
    }
}
