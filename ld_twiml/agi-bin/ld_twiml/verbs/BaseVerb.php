<?php
namespace Verbs;


class BaseVerb
{
    public static $SUPPORTED_CHILDREN = array();

    /**
     * Init the class with an xml node
     *
     * @param DOMNode $node xml node to init the class with
     */
    public function __construct($node)
    {
        $has_children = false;

        // Check child nodes and if we find even one that we support then assume that we
        // should be using child nodes instead of the parent node
        if ($node->hasChildNodes()) {
            foreach ($node->childNodes as $child) {
                if (array_key_exists(strtolower($child->nodeName), static::$SUPPORTED_CHILDREN)) {
                    $has_children = true;
                    break;
                }
            }
        }

        $node_data = $has_children ? self::unpackNodeWithChildren($node) : self::unpackNode($node);

        $this->has_children = $has_children;
        $this->setData($node_data);
    }

    /**
     * Unpack an xml node to an array. Ignores child nodes 
     *
     * @param DOMNode $node xml node to unpack
     * @param bool $without_value should the results contain value of the node
     * @return array containing node name, attributes and optionally value
     */
    private function unpackNode($node, $include_value=true)
    {
        $attributes = array();

        if ($node->hasAttributes()) {
            foreach ($node->attributes as $attribute) {
                $attributes[strtolower($attribute->nodeName)] = $attribute->nodeValue;
            }
        }

        $results = array(
            "name" => strtolower($node->nodeName),
            "attrs" => $attributes
        );

        if ($include_value === true) {
            $results["value"] = str_replace("\n", " ", $node->nodeValue);
        }

        return $results;
    }

    /**
     * Unpack xml node to an array. Also unpacks direct child nodes
     *
     * @param DOMNode $node xml node to unpack
     * @return array containing node name, its attributes and children nodes and their names, attributes and values
     */
    private function unpackNodeWithChildren($node)
    {
        // Get node data. Note that we do not get the node's value since the node is supposed to have child nodes
        $results = self::unpackNode($node, false);

        $results["children"] = array();
        $base_child = null;

        // Note that we only support child nodes with the same type, you cannot mix <Number> and <Queue> for example
        // First node's type is used as a base for the comparison and we will discard remaining child nodes if:
        // 1. We reach maximum amount of children that we support for that node type
        // 2. We encounter some other node type
        if ($node->hasChildNodes()) {
            foreach ($node->childNodes as $child_node) {
                // Skip the nodes that we do not support
                if (!array_key_exists(strtolower($child_node->nodeName), static::$SUPPORTED_CHILDREN)) {
                    continue;
                }

                // Set the base once we find the first supported node type
                if (is_null($base_child)) {
                    $base_child = strtolower($child_node->nodeName);
                // Stop iterating if we find some other node type or if we have reached the max
                } elseif (
                    strtolower($child_node->nodeName) !== $base_child
                    || count($results["children"]) >= static::$SUPPORTED_CHILDREN[$base_child]["max_consecutive"]
                ) {
                    break;
                }

                $results["children"][] = self::unpackNode($child_node);
            }
        }

        return $results;
    }

    /**
     * Set all different data values that get extracted from the xml node during unpacking
     *
     * @param array $node_data results of an unpacked xml node
     * @return null
     */
    private function setData($node_data)
    {
        $this->node_data = $node_data;
        $this->name = $node_data["name"];
        $this->attributes = $node_data["attrs"];
        // No value if there's children
        $this->value = $this->hasChildren() ? null : $node_data["value"];
        $this->children = $this->hasChildren() ? $node_data["children"] : null;
        $this->children_type = $this->hasChildren() ? $node_data["children"][0]["name"] : null;
    }

    /**
     * Check if this node instance has child nodes
     *
     * @return bool telling if the node has child nodes or not
     */
    public function hasChildren()
    {
        return $this->has_children;
    }

    /**
     * Get name of the child nodes
     *
     * @return string|null name of the child nodes if there are any, null if there's no children
     */
    public function getChildrenType()
    {
        return $this->children_type;
    }

    /**
     * Get value for the given attribute name
     *
     * @param string $attribute_name name of the attribute to look for
     * @return string|null value of the attribute as a string if found, otherwise null
     */
    public function getAttribute($attribute_name)
    {
        if (array_key_exists($attribute_name, $this->attributes)) {
            return $this->attributes[$attribute_name];
        } else {
            return null;
        }
    }

    /**
     * Get all data associated with the node
     *
     * @return array containing all data that the node has
     */
    public function getNodeData()
    {
        return $this->node_data;
    }

    /**
     * Get name of the node
     *
     * @return string node name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get attributes of the node
     *
     * @return array containing attribute names and corresponding values
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Get value of the node
     *
     * @return string|null node value as a string if node didn't have any child nodes, null if there's child nodes
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get node's child nodes
     *
     * @return array|null array containing all the data about child nodes (name, value, attributes) or null if there's no child nodes
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get all values defined either in the node itself or in its child nodes
     *
     * @return array containing the node value or values of its children
     */
    public function getAllValues()
    {
        $values = array();

        if (!is_null($this->value)) {
            $values[] = $this->value;
        } else {
            foreach ($this->children as $child) {
                $values[] = $child["value"];
            }
            
        }

        return $values;
    }
}
