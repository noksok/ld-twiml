<?php
namespace Utils;


class AgiCommand
{
    
    public function __construct($agi)
    {
        $this->agi = $agi;
        $this->callAnswered = false;
        $this->callerIdChanged = false;
    }

    /**
     * Send a command to Asterisk
     *
     * @param string $command command to be sent
     * @return array containing the results from agispeedy
     */
    public function send($command)
    {
        return $this->agi->evaluate($command);
    }

    /**
     * Shortcut for sending an answer call command 
     *
     * @return array containing the results from agispeedy
     */
    public function answerCall()
    {
        $result = null;

        if ($this->callAnswered === false) {
            $this->log("Call not yet answered, answering...");
            $result = $this->agi->answer();
            $this->callAnswered = true;
        }

        return $result;
    }

    /**
     * Shortcut for sending a hangup call command 
     *
     * @return array containing the results from agispeedy
     */
    public function hangupCall()
    {
        return $this->agi->hangup();
    }

    /**
     * Shortcut for sending a logging command to Asterisk. Given message will be logged into Asterisk full log
     *
     * @param string $message log entry to be written
     * @param int $level log level to be used 
     * @return null
     */
    public function log($message, $level=1)
    {
        if (!is_numeric($level)) {
            $level = 1;
        }

        $command = "VERBOSE \"{$message}\" {$level}";

        return $this->send($command);
    }

    /**
     * Shortcut for overwriting the caller id
     *
     * @param string $phone_number number that will be set as the caller id
     * @return null
     */
    public function setCallerId($phoneNumber)
    {
        // List of variables that need to be overwritten in order to take a new caller id into use
        $asterisk_variables = array("CALLERID(num)", "CALLERID(ani)", "CALLERID(name)");

        foreach ($asterisk_variables as $asterisk_variable) {
            $this->send("SET VARIABLE {$asterisk_variable} \"{$phoneNumber}\"");
        }
    }

    /**
     * Play a sound file
     *
     * @param string $baseFile path to the file without the file extension
     * @param int $timeout how long to wait after the file has been played
     * @return array containing the results from agispeedy
     */
    public function playFile($baseFile, $timeout=0)
    {
        $command = "GET OPTION \"{$baseFile}\" \"0123456789#*\" {$timeout}";
        return $this->send($command);
    }

    /**
     * Wait silently
     *
     * @param int $timeout how long to wait
     * @return array containing the results from agispeedy
     */
    public function wait($timeout)
    {
        return $this->agi->wait_for_digit($timeout);
    }

    /**
     * Add digits at the end of the phone number
     *
     * @param string $calledFrom caller's number
     * @param string $postfix string to add at the end of the caller's number
     * @return null
     */
    public function addPostfix($calledFrom, $postfix)
    {
        // First fix the number back to the original value if it was changed previously
        if ($this->callerIdChanged === true) {
            $this->setCallerId($calledFrom);
            $this->log("Caller id set back to the original value: {$calledFrom}");
            // Revert the flag once the number has been reverted
            $this->callerIdChanged = false;
        }
        // Change the caller id if postfix value has been provided
        if (!is_null($postfix) && is_numeric(str_replace("+", "", $calledFrom))) {
            $newCallerId = $calledFrom . $postfix;
            $this->setCallerId($newCallerId);
            $this->log("Caller id set to: {$newCallerId}");
            $this->callerIdChanged = true;
        }
    }

    /**
     * Set language for the channel
     *
     * @param string $language value for the new language
     * @return array containing the results from agispeedy
     */
    public function setLanguage($language)
    {
        return $this->agi->exec_setlanguage($language);
    }
}
