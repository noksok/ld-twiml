<?php
namespace Utils;

class XmlLogger
{
    private static $LOG_FILE_PATH = "/var/log/ld_twiml/actions.log";

    public function __construct($callId, $uniqueId, $clientId, $calledFrom, $calledTo)
    {
        $this->callId = $callId;
        $this->uniqueId = $uniqueId;
        $this->clientId = $clientId;
        $this->calledFrom = $calledFrom;
        $this->calledTo = $calledTo;
    }

    public function write($data)
    {
        $date = new \DateTime("now", new \DateTimeZone("Europe/Helsinki"));
        $now = $date->format("c");

        // To support the old naming
        if (array_key_exists("attributes", $data) && !array_key_exists("attrs", $data)) {
            $data["attrs"] = $data["attributes"];
            unset($data["attributes"]);
        }

        $data = json_encode(array(
            "sip_call_id" => $this->callId,
            "ast_unique_id" => $this->uniqueId,
            "client_id" => $this->clientId,
            "anum" => $this->calledFrom,
            "bnum" => $this->calledTo,
            "timestamp" => $now,
            "action" => $data
        ));

        // Write to the action log and to asterisk full log
        $file = fopen(self::$LOG_FILE_PATH, "a");
        fwrite($file, $data . "\n");
        fclose($file);
    }

    public function writeFromXml($xmlNode)
    {
        $name = $xmlNode->nodeName;
        $value = $xmlNode->nodeValue;

        $attributes = array();

        if ($xmlNode->hasAttributes()) {
            foreach ($xmlNode->attributes as $attribute) {
                $attributes[$attribute->nodeName] = $attribute->nodeValue;
            }
        }

        $extracted = array(
            "name" => $name,
            "value" => $value,
            "attributes" => $attributes,
        );

        $this->write($extracted);
    }
}
