<?php
namespace Utils;

function getAbsoluteUrl($relativeUrl, $baseUrl){

    // if already absolute URL 
    if (parse_url($relativeUrl, PHP_URL_SCHEME) !== null) {
        return $relativeUrl;
    }
    
    // queries and anchors
    if ($relativeUrl[0] === '#' || $relativeUrl[0] === '?') {
        return $baseUrl.$relativeUrl;
    }
    
    // parse base URL and convert to: $scheme, $host, $path, $query, $port, $user, $pass
    extract(parse_url($baseUrl));
    
    // if base URL contains a path remove non-directory elements from $path
    if (isset($path) === true) {
        $path = preg_replace('#/[^/]*$#', '', $path);
    }
    else {
        $path = '';
    }
    
    // if realtive URL starts with //
    if (substr($relativeUrl, 0, 2) === '//') {
        return $scheme.':'.$relativeUrl;
    }
    
    // if realtive URL starts with /
    if ($relativeUrl[0] === '/') {
        $path = null;
    }
    
    $abs = null;
    
    // if realtive URL contains a user
    if (isset($user) === true) {
        $abs .= $user;
    
        // if realtive URL contains a password
        if (isset($pass) === true){
            $abs .= ":" . $pass;
        }
        
        $abs .= "@";
    }
    
    $abs .= $host;
    
    // if realtive URL contains a port
    if (isset($port) === true) {
        $abs .= ":".$port;
    }
    
    $abs .= $path . "/" . $relativeUrl.(isset($query) === true ? ("?" . $query) : null);
    
    // replace // or /./ or /foo/../ with /
    //$re = ['#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#'];
    $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
    for ($n = 1; $n > 0; $abs = preg_replace($re, '/', $abs, -1, $n)) {
    }
    
    // return absolute URL
    return $scheme . "://" . $abs;
}
