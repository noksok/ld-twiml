<?php
namespace Utils;

class SoundFiles
{
    public static function downloadFile($filepath, $url)
    {
        $file = fopen($url, "rb");
        if ($file) {
            $downloadedFile = fopen ($filepath, "wb");
            if ($downloadedFile) {
                while(!feof($file)) {
                    fwrite($downloadedFile, fread($file, 1024 * 8 ), 1024 * 8 );
                }

                fclose($downloadedFile);
            }

            fclose($file);
        }

    }

    public static function getAndConvert($baseFile, $url)
    {
        $mp3File = $baseFile . ".mp3";
        
        self::downloadFile($mp3File, $url);
        self::convertFileToAlawAndUlaw($baseFile, $mp3File);

    }

    public static function filesExist($baseFile)
    {
        $alawFile = $baseFile . ".alaw";
        $ulawFile = $baseFile . ".ulaw";

        if (file_exists($alawFile) && file_exists($ulawFile)) {
            return true;
        } else {
            return false;
        }
    }

    public static function convertFileToAlawAndUlaw($baseFile, $mp3File)
    {
        $alawFile = $baseFile . ".al";
        $ulawFile = $baseFile . ".ul";
        $alawFileFixed = $baseFile . ".alaw";
        $ulawFileFixed = $baseFile . ".ulaw";

        // Try to get the amount of channels from the file info, default to 1 channel
        // Converted files can sound pretty broken if incorrect channel number is used
        $channels = 1;
        exec("/opt/sox/bin/sox --i {$mp3File}", $fileInfo);
        foreach ($fileInfo as $item) {
            if (strpos($item, "Channels") !== false) {
                $channels = $item[strlen($item) - 1];
                break;
            }
        }

        // Do the conversions and then "fix" the file formats from .al -> .alaw and .ul -> .ulaw
        shell_exec("/opt/sox/bin/sox -c {$channels} -r 8000 {$mp3File} -c 1 {$alawFile} rate 8000");
        shell_exec("/opt/sox/bin/sox -c {$channels} -r 8000 {$mp3File} -c 1 {$ulawFile} rate 8000");
        shell_exec("/bin/mv {$alawFile} {$alawFileFixed}");
        shell_exec("/bin/mv {$ulawFile} {$ulawFileFixed}");

        // Get rid of the mp3
        unlink($mp3File);
    }
}
