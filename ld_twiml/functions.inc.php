<?php
if (!defined('FREEPBX_IS_AUTH')) { die('No direct script access allowed'); }
//This file is part of LeadDesk.

function ld_twiml_destinations() {
    global $module_page;
    $extens = array();

    // return an associative array with destination and description
    foreach (ld_twiml_list() as $row) {
        $extens[] = array(
            'destination' => 'app-ld_twiml,'.$row['ld_twiml_id'].',1',
            'description' => $row['description']
        );
    }
    return $extens;
}

function ld_twiml_getdest($id) {
    return array("app-ld_twiml,$id,1");
}

function ld_twiml_getdestinfo($dest) {
        if (substr(trim($dest),0,13) == 'app-ld_twiml,') {
                $grp = explode(',',$dest);
                $id = $grp[1];
                $thisext = ld_twiml_get($id);
                if (empty($thisext)) {
                        return array();
                } else {
                        return array('description' => sprintf(_("LeadDesk TwiML %s: "),$thisext['description']),
                                     'edit_url' => 'config.php?display=ld_twiml&id='.urlencode($id),
                        );
                }
        } else {
                return false;
        }
}

function ld_twiml_get_config($engine) {
    global $ext;
    switch ($engine) {
    case 'asterisk':
        foreach (ld_twiml_list() as $row) {
            $context = 'app-ld_twiml';
            $id = $row['ld_twiml_id'];

            // Use variable inheritance (var name starts with __) so that client id will be passed to any new channels
            // It can then be used for determining the client if the call is tranferred out
            $ext->add($context, $id, '', new ext_setvar('__CLIENTIDINHERITED', $row['client_id']));
            $ext->add($context, $id, '', new ext_setvar('CALLID', '${SIP_HEADER(Call-ID)}'));

            $ext->add($context, $id, '', new ext_noop('Starting TwiML processing '.$row['description'].' to client '.$row['client_id']));
            $ext->add($context, $id, '', new ext_agi('agi://127.0.0.1/twiml, protocol=' . $row['protocol'] . ', url=' . $row['url'] . ', client_id=' . $row['client_id'] . ', auth=' . $row['auth'] . ', call_id=${CALLID}'));

            $ext->add($context, $id, 'end', new ext_goto($row['post_dest']));
        }
        break;
    }
}

function ld_twiml_list() {
    global $db;
    $sql = "SELECT ld_twiml_id, description, client_id, post_dest, protocol, url, auth FROM ld_twiml";
    $results = $db->getAll($sql, DB_FETCHMODE_ASSOC);
    if(DB::IsError($results)) {
        die_freepbx($results->getMessage()."<br><br>Error selecting from ld_twiml");
    }

    return $results;
}

function ld_twiml_get($ld_twiml_id) {
    global $db;
    $sql = "SELECT ld_twiml_id, description, protocol, url, auth, client_id, post_dest FROM ld_twiml WHERE ld_twiml_id = '".$db->escapeSimple($ld_twiml_id)."'";
    $row = $db->getRow($sql,DB_FETCHMODE_ASSOC);
    if(DB::IsError($row)) {
        die_freepbx($row->getMessage()."<br><br>Error selecting row from ld_twiml");
    }
    return $row;
}

function ld_twiml_add($description, $client_id, $protocol, $url, $auth, $post_dest) {
    global $db;
    $sql = "INSERT INTO ld_twiml (description, client_id, protocol, url, auth, post_dest) VALUES (".
        "'".$db->escapeSimple($description)."', ".
        "'".$db->escapeSimple($client_id)."', ".
        "'".$db->escapeSimple($protocol)."', ".
        "'".$db->escapeSimple($url)."', ".
        "'".$db->escapeSimple($auth)."', ".
        "'".$db->escapeSimple($post_dest)."')";
    $result = $db->query($sql);
    if(DB::IsError($result)) {
        die_freepbx($result->getMessage().$sql);
    }
}

function ld_twiml_delete($ld_twiml_id) {
    global $db;
    $sql = "DELETE FROM ld_twiml WHERE ld_twiml_id = ".$db->escapeSimple($ld_twiml_id);
    $result = $db->query($sql);
    if(DB::IsError($result)) {
        die_freepbx($result->getMessage().$sql);
    }
}

function ld_twiml_edit($ld_twiml_id, $description, $client_id, $protocol, $url, $auth, $post_dest) {
    global $db;
    $sql = "UPDATE ld_twiml SET ".
        "description = '".$db->escapeSimple($description)."', ".
        "client_id = '".$db->escapeSimple($client_id)."', ".
        "protocol = '".$db->escapeSimple($protocol)."', ".
        "url = '".$db->escapeSimple($url)."', ".
        "auth = '".$db->escapeSimple($auth)."', ".
        "post_dest = '".$db->escapeSimple($post_dest)."' ".
        "WHERE ld_twiml_id = ".$db->escapeSimple($ld_twiml_id);
    $result = $db->query($sql);
    if(DB::IsError($result)) {
        die_freepbx($result->getMessage().$sql);
    }
}

function ld_twiml_check_destinations($dest=true) {
    $destlist = array();
    if (is_array($dest) && empty($dest)) {
        return $destlist;
    }
    $sql = "SELECT ld_twiml_id, post_dest, description FROM ld_twiml ";
    if ($dest !== true) {
        $sql .= "WHERE post_dest in ('".implode("','",$dest)."')";
    }
    $results = sql($sql,"getAll",DB_FETCHMODE_ASSOC);

    foreach ($results as $result) {
        $thisdest = $result['post_dest'];
        $thisid   = $result['ld_twiml_id'];
        $destlist[] = array(
            'dest' => $thisdest,
            'description' => 'LeadDesk TwiML: '.$result['description'],
            'edit_url' => 'config.php?display=ld_twiml&type=tool&id='.urlencode($thisid),
        );
    }
    return $destlist;
}

function ld_twiml_change_destination($old_dest, $new_dest) {
    $sql = 'UPDATE ld_twiml SET post_dest = "' . $new_dest . '" WHERE post_dest= "' . $old_dest . '"';
    sql($sql, "query");
}
