<?php 
if (!defined('FREEPBX_IS_AUTH')) { die('No direct script access allowed'); }
//This file is part of LeadDesk.

$action = isset($_POST['action']) ? $_POST['action'] :  '';
if (isset($_POST['delete'])) $action = 'delete'; 

$tabindex = 0;


$ld_twiml_id = isset($_POST['ld_twiml_id']) ? $_POST['ld_twiml_id'] :  false;
$description = isset($_POST['description']) ? $_POST['description'] :  '';
$client_id = isset($_POST['client_id']) ? $_POST['client_id'] :  '0';
$url = isset($_POST['url']) ? $_POST['url'] : '';
$protocol = isset($_POST['protocol']) ? $_POST['protocol'] : '';
$auth = isset($_POST['auth']) ? $_POST['auth'] : '';
$post_dest = isset($_POST['post_dest']) ? $_POST['post_dest'] :  '';

if (isset($_POST['goto0']) && $_POST['goto0']) {
	// 'ringgroup_post_dest'  'ivr_post_dest' or whatever
	$post_dest = $_POST[ $_POST['goto0'].'0' ];
}


switch ($action) {
	case 'add':
		ld_twiml_add($description, $client_id, $protocol, $url, $auth, $post_dest);
		needreload();
		redirect_standard();
	break;
	case 'edit':
		ld_twiml_edit($ld_twiml_id, $description, $client_id, $protocol, $url, $auth, $post_dest);
		needreload();
		redirect_standard('extdisplay');
	break;
	case 'delete':
		ld_twiml_delete($ld_twiml_id);
		needreload();
		redirect_standard();
	break;
}


?> 

<div class="rnav"><ul>
<?php 
echo '<li><a href="config.php?display=ld_twiml&amp;type=setup">'._('Add LeadDesk TwiML').'</a></li>';

foreach (ld_twiml_list() as $row) {
	echo '<li><a href="config.php?display=ld_twiml&amp;type=setup&amp;extdisplay='.$row['ld_twiml_id'].'" class="">'.$row['description'].'</a></li>';
}

?>
</ul></div>

<?php
if ($extdisplay) {
	// load
	$row = ld_twiml_get($extdisplay);
	$description = $row['description'];
	$client_id = $row['client_id'];
	$url = $row['url'];
	$auth = $row['auth'];
	$protocol = $row['protocol'];	
	$post_dest = $row['post_dest'];
}

?>
<form name="editLdTwiML" action="<?php  $_SERVER['PHP_SELF'] ?>" method="post" onsubmit="return checkLdtwiml(editLdTwiML);">
	<input type="hidden" name="extdisplay" value="<?php echo $extdisplay; ?>">
	<input type="hidden" name="ld_twiml_id" value="<?php echo $extdisplay; ?>">
	<input type="hidden" name="action" value="<?php echo ($extdisplay ? 'edit' : 'add'); ?>">
	<table>
		<tr><td colspan="2"><h5><?php  echo ($extdisplay ? _("Edit LeadDesk TwiML") : _("Add LeadDesk TwiML")) ?><hr></h5></td></tr>
		<tr>
			<td><a href="#" class="info"><?php echo _("Description")?>:<span><?php echo _("The name of this TwiML")?></span></a></td>
			<td><input size="15" type="text" name="description" value="<?php  echo $description; ?>" tabindex="<?php echo ++$tabindex;?>"></td>
		</tr>
		<tr>
			<td><a href="#" class="info"><?php echo _("Client ID")?><span><?php echo _("The client's ID for billing")?></span></a></td>
			<td><input size="15" type="text" name="client_id" value="<?php echo $client_id; ?>" tabindex="<?php echo ++$tabindex;?>"></td>
		</tr>
		<tr>
			<td><a href="#" class="info"><?php echo _("protocol")?><span><?php echo _("Protocol (HTTP/HTTPS) for the TwiML document.")?></span></a></td>
			<td><input size="10" type="text" name="protocol" value="<?php echo $protocol; ?>" tabindex="<?php echo ++$tabindex;?>"></td>
		</tr>
		<tr>
			<td><a href="#" class="info"><?php echo _("url")?><span><?php echo _("URL for the TwiML.")?></span></a></td>
			<td><input size="255" type="text" name="url" value="<?php echo $url; ?>" tabindex="<?php echo ++$tabindex;?>"></td>
		</tr>
		<tr>
			<td><a href="#" class="info"><?php echo _("auth")?><span><?php echo _("Client Auth")?></span></a></td>
			<td><input size="32" type="text" name="auth" value="<?php echo $auth; ?>" tabindex="<?php echo ++$tabindex;?>"></td>
		</tr>

		<tr><td colspan="2"><br><h5><?php echo _("Destination in case of failure to run TwiML")?>:<hr></h5></td></tr>
		<?php 
		//draw goto selects
		echo drawselects($post_dest,0);
		?>
		<tr>
			<td colspan="2"><br><input name="Submit" type="submit" value="<?php echo _("Submit Changes")?>" tabindex="<?php echo ++$tabindex;?>">
				<?php if ($extdisplay) { echo '&nbsp;<input name="delete" type="submit" value="'._("Delete").'">'; } ?>
			</td>		
			
		</tr>
		<?php
		if ($extdisplay) {
			$usage_list = framework_display_destination_usage(ld_twiml_getdest($extdisplay));
			if (!empty($usage_list)) {
		?>
		<tr>
			<td colspan="2"><a href="#" class="info"><?php echo $usage_list['text']?>:<span><?php echo $usage_list['tooltip']?></span></a></td>
		</tr>
		<?php
			}
		}
		?>
	</table>
</form>
			
			
<script language="javascript">
<!--

function checkLdtwiml(theForm) {
	var msgInvalidDescription = "<?php echo _('Invalid description specified'); ?>";

	// set up the Destination stuff
	setDestinations(theForm, '_post_dest');

	// form validation
	defaultEmptyOK = false;	
	if (isEmpty(theForm.description.value))
		return warnInvalid(theForm.description, msgInvalidDescription);

	if (!validateDestinations(theForm, 1, true))
		return false;

	return true;
}
//-->
</script>

