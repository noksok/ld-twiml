<?php
use PHPUnit\Framework\TestCase;


final class DialTest extends TestCase
{
    public function testSetupDialWithoutChildren()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial", "+3581234567");

        $attr = $doc->createAttribute("timeout");
        $attr->value = "10";
        $node->appendChild($attr);

        $instance = new \Verbs\Dial($node);

        $this->assertFalse($instance->hasChildren());
        $this->assertEquals($instance->getAttribute("timeout"), "10");
        $this->assertEquals($instance->getName(), "dial");
        $this->assertEquals($instance->getValue(), "+3581234567");
        $this->assertNull($instance->getChildren());
        $this->assertNull($instance->getChildrenType());

        $node_data = $instance->getNodeData();
        $this->assertEquals($node_data["name"], "dial");
        $this->assertEquals($node_data["value"], "+3581234567");
        $this->assertEquals($node_data["attrs"]["timeout"], "10");
        $this->assertArrayNotHasKey("children", $node_data);

        $attributes = $instance->getAttributes();
        $this->assertCount(1, $attributes);
        $this->assertEquals($attributes["timeout"], "10");

        $values = $instance->getAllValues();
        $this->assertCount(1, $values);
        $this->assertEquals($values[0], "+3581234567");

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC DIAL local/003581234567@from-internal,10");
    }

    public function testSetupInstanceWitChildren()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $num1 = $doc->createElement("Number", "+3581234567");
        $attr = $doc->createAttribute("sendDigits");
        $attr->value = "111";
        $num1->appendChild($attr);
        $node->appendChild($num1);

        $num2 = $doc->createElement("Number", "+3587654321");
        $attr = $doc->createAttribute("sendDigits");
        $attr->value = "222";
        $num2->appendChild($attr);
        $node->appendChild($num2);

        $attr = $doc->createAttribute("timeout");
        $attr->value = "10";
        $node->appendChild($attr);

        $instance = new \Verbs\Dial($node);

        $this->assertTrue($instance->hasChildren());
        $this->assertNull($instance->getAttribute("some bogus atttribute"));
        $this->assertEquals($instance->getAttribute("timeout"), "10");

        $node_data = $instance->getNodeData();
        $this->assertEquals($node_data["name"], "dial");
        $this->assertArrayNotHasKey("value", $node_data);
        $this->assertCount(1, $node_data["attrs"]);
        $this->assertEquals($node_data["attrs"]["timeout"], "10");
        $this->assertCount(2, $node_data["children"]);
        $this->assertEquals($node_data["children"][0]["name"], "number");
        $this->assertEquals($node_data["children"][0]["value"], "+3581234567");
        $this->assertCount(1, $node_data["children"][0]["attrs"]);
        $this->assertEquals($node_data["children"][0]["attrs"]["senddigits"], "111");
        $this->assertEquals($node_data["children"][1]["name"], "number");
        $this->assertEquals($node_data["children"][1]["value"], "+3587654321");
        $this->assertCount(1, $node_data["children"][1]["attrs"]);
        $this->assertEquals($node_data["children"][1]["attrs"]["senddigits"], "222");

        $this->assertEquals($instance->getName(), "dial");
        $this->assertEquals($instance->getChildrenType(), "number");

        $attributes = $instance->getAttributes();
        $this->assertCount(1, $attributes);
        $this->assertEquals($attributes["timeout"], "10");

        $this->assertNull($instance->getValue());

        $children = $instance->getChildren();
        $this->assertCount(2, $children);
        $this->assertEquals($children[0]["name"], "number");
        $this->assertEquals($children[0]["value"], "+3581234567");
        $this->assertCount(1, $children[0]["attrs"]);
        $this->assertEquals($children[0]["attrs"]["senddigits"], "111");
        $this->assertEquals($children[1]["name"], "number");
        $this->assertEquals($children[1]["value"], "+3587654321");
        $this->assertCount(1, $children[1]["attrs"]);
        $this->assertEquals($children[1]["attrs"]["senddigits"], "222");

        $values = $instance->getAllValues();
        $this->assertCount(2, $values);
        $this->assertEquals($values[0], "+3581234567");
        $this->assertEquals($values[1], "+3587654321");
    }

    public function testTooSmallTimeout()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $attr = $doc->createAttribute("timeout");
        $attr->value = "1";
        $node->appendChild($attr);

        $instance = new \Verbs\Dial($node);

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC DIAL local/003581234567@from-internal,5");
    }

    public function testTooBigTimeout()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $attr = $doc->createAttribute("timeout");
        $attr->value = "1000";
        $node->appendChild($attr);

        $instance = new \Verbs\Dial($node);

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC DIAL local/003581234567@from-internal,600");
    }

    public function testInvalidTimeout()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $attr = $doc->createAttribute("timeout");
        $attr->value = "this is not a valid number";
        $node->appendChild($attr);

        $instance = new \Verbs\Dial($node);

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC DIAL local/003581234567@from-internal,30");
    }

    public function testMissingTimeout()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $instance = new \Verbs\Dial($node);

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC DIAL local/003581234567@from-internal,30");
    }

    public function testNumber()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $instance = new \Verbs\Dial($node);

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC DIAL local/003581234567@from-internal,30");
    }

    public function testQueue()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $queue = $doc->createElement("Queue", "9999123456789");
        $node->appendChild($queue);

        $instance = new \Verbs\Dial($node);

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC Goto ext-queues,9999123456789,1");
    }

    public function testTooManyChildren()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $queue1 = $doc->createElement("Queue", "9999123456789");
        $node->appendChild($queue1);

        $queue2 = $doc->createElement("Queue", "9999987654321");
        $node->appendChild($queue2);

        $instance = new \Verbs\Dial($node);

        $children = $instance->getChildren();
        $this->assertCount(1, $children);
        $this->assertEquals($children[0]["name"], "queue");
        $this->assertEquals($children[0]["value"], "9999123456789");
        $this->assertEmpty($children[0]["attrs"]);

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC Goto ext-queues,9999123456789,1");
    }

    public function testMixedChildren()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $queue = $doc->createElement("Queue", "9999123456789");
        $node->appendChild($queue);

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $instance = new \Verbs\Dial($node);

        $children = $instance->getChildren();
        $this->assertCount(1, $children);
        $this->assertEquals($children[0]["name"], "queue");
        $this->assertEquals($children[0]["value"], "9999123456789");
        $this->assertEmpty($children[0]["attrs"]);

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC Goto ext-queues,9999123456789,1");
    }

    public function testGetDialCallStatusForAsteriskDialResult()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $instance = new \Verbs\Dial($node);

        $this->assertEquals($instance->getDialCallStatusForAsteriskDialResult(0), "no-answer");
        $this->assertNull($instance->getDialCallStatusForAsteriskDialResult("this doesn't exist"));
    }

    public function testDialToMultipleNumbers()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $attr = $doc->createAttribute("timeout");
        $attr->value = "100";
        $node->appendChild($attr);

        $num1 = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num1);

        $num2 = $doc->createElement("Number", "003582345678");
        $node->appendChild($num2);

        $instance = new \Verbs\Dial($node);

        $this->assertEquals($instance->getAsteriskCommand(), "EXEC DIAL local/003581234567@from-internal&local/003582345678@from-internal,100");
    }

    public function testValidCallerIdPostfix()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $attr = $doc->createAttribute("callerIdPostfix");
        $attr->value = "55";
        $node->appendChild($attr);

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $instance = new \Verbs\Dial($node);

        $this->assertEquals($instance->getCallerIdPostfix(), "55");
    }

    public function testTooShortCallerIdPostfix()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $attr = $doc->createAttribute("callerIdPostfix");
        $attr->value = "1";
        $node->appendChild($attr);

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $instance = new \Verbs\Dial($node);

        $this->assertNull($instance->getCallerIdPostfix());
    }

    public function testTooLongCallerIdPostfix()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $attr = $doc->createAttribute("callerIdPostfix");
        $attr->value = "666";
        $node->appendChild($attr);

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $instance = new \Verbs\Dial($node);

        $this->assertNull($instance->getCallerIdPostfix());
    }

    public function testInvalidCallerIdPostfix()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $attr = $doc->createAttribute("callerIdPostfix");
        $attr->value = "0p";
        $node->appendChild($attr);

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $instance = new \Verbs\Dial($node);

        $this->assertNull($instance->getCallerIdPostfix());
    }

    public function testNegativeCallerIdPostfix()
    {
        $doc = new \DOMDocument("1.0", "UTF-8");
        $node = $doc->createElement("Dial");

        $attr = $doc->createAttribute("callerIdPostfix");
        $attr->value = "-1";
        $node->appendChild($attr);

        $num = $doc->createElement("Number", "+3581234567");
        $node->appendChild($num);

        $instance = new \Verbs\Dial($node);

        $this->assertNull($instance->getCallerIdPostfix());
    }
}
